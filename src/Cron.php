<?php

declare(strict_types = 1);

namespace Drupal\commerce_alma;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Alma\API\Entities\Payment as RemotePayment;

/**
 * Provides cron routines.
 *
 * @see \Drupal\commerce_alma\Plugin\QueueWorker\PaymentUpdater
 */
class Cron {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Constructs a new Cron instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueueFactory $queue_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->queueFactory = $queue_factory;
  }

  /**
   * Enqueues in progess Alma payment for an update.
   */
  public function run() {
    $queue = $this->queueFactory->get('commerce_alma_payment_updater');

    // Wait for the queue to be guessed as empty before adding new items
    // to avoid the queue to infinitly grow.
    /** @see https://www.drupal.org/project/commerce_alma/issues/3440443 */
    if ($queue->numberOfItems()) {
      return;
    }

    $payment_gateways = $this->entityTypeManager
      ->getStorage('commerce_payment_gateway')
      ->getQuery()
      ->accessCheck()
      ->condition('plugin', 'alma')
      ->execute();

    if (!$payment_gateway_ids) {
      return;
    }

    $payment_gateways = $this->entityTypeManager->getStorage('commerce_payment_gateway')->loadMultiple($payment_gateway_ids);
    $payment_gateway_ids = array_filter($payment_gateway_ids, function ($payment_gateway_id) use ($payment_gateways) {
      return $payment_gateways[$payment_gateway_id]->getPlugin()->getConfiguration()['update_payments'];
    });

    if (!$payment_gateway_ids) {
      return;
    }

    $payments = $this->entityTypeManager
      ->getStorage('commerce_payment')
      ->loadByProperties([
        'remote_state' => RemotePayment::STATE_IN_PROGRESS,
        'payment_gateway.target_id' => $payment_gateway_ids,
      ]);

    foreach ($payments as $payment) {
      $queue->createItem($payment);
    }
  }

}
