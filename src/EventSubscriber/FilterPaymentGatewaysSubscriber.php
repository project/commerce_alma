<?php

declare(strict_types = 1);

namespace Drupal\commerce_alma\EventSubscriber;

use Alma\API\RequestError;
use Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent;
use Drupal\commerce_payment\Event\PaymentEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Filter out Alma gateways according to order eligilbility.
 */
class FilterPaymentGatewaysSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      PaymentEvents::FILTER_PAYMENT_GATEWAYS => 'onFilter',
    ];
  }

  /**
   * Filters out Alma payment gateways for non eligible orders.
   *
   * @param \Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent $event
   *   The event.
   */
  public function onFilter(FilterPaymentGatewaysEvent $event) {
    $payment_gateways = $event->getPaymentGateways();
    foreach ($payment_gateways as $payment_gateway_id => $payment_gateway) {
      // Act on alma gateways only.
      if ($payment_gateway->getPluginId() !== 'alma') {
        continue;
      }

      // Alma only accepts payments in euros.
      $order = $event->getOrder();
      if ($order->getTotalPrice()->getCurrencyCode() !== 'EUR') {
        unset($payment_gateways[$payment_gateway_id]);
        $event->setPaymentGateways($payment_gateways);
        return;
      }

      $payment_gateway_plugin = $payment_gateway->getPlugin();
      // Unset plans for which the order is not eligible.
      try {
        /** @var \Drupal\commerce_alma\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
        $elegibilities = $payment_gateway_plugin->checkOrderEligibility($order);
        $eligible = array_filter($elegibilities, function ($elegibility) {
          return $elegibility->isEligible();
        });

        if (!$eligible) {
          unset($payment_gateways[$payment_gateway_id]);
        }
      }
      // …or for which the eligibility can't be established.
      catch (RequestError $e) {
        unset($payment_gateways[$payment_gateway_id]);
      }
    }

    $event->setPaymentGateways($payment_gateways);
  }

}
