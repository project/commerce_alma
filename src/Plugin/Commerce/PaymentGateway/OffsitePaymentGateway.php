<?php

declare(strict_types = 1);

namespace Drupal\commerce_alma\Plugin\Commerce\PaymentGateway;

use Alma\API\Client;
use Drupal\Core\Url;
use Alma\API\ParamsError;
use Alma\API\RequestError;
use Drupal\commerce_price\Price;
use Drupal\commerce_alma\Event\Events;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Alma\API\Entities\PaymentPlanInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Render\MarkupInterface;
use Symfony\Component\HttpFoundation\Request;
use Alma\API\Entities\Payment as RemotePayment;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_alma\Event\CreatePaymentEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Provides the Alma offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "alma",
 *   label = @Translation("Alma"),
 *   display_label = @Translation("Alma"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_alma\PluginForm\RedirectCheckoutForm",
 *   },
 * )
 */
class OffsitePaymentGateway extends OffsitePaymentGatewayBase implements ContainerFactoryPluginInterface {

  const REFUNDABLE_PAYMENT_STATES = ['completed', 'partially_refunded'];

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The modules logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Alma API client.
   *
   * @var \Alma\API\Client
   */
  protected $api;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, string $plugin_id, mixed $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, MinorUnitsConverterInterface $minor_units_converter = NULL, EventDispatcherInterface $event_dispatcher, LoggerChannelFactoryInterface $logger_channel_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);

    $this->eventDispatcher = $event_dispatcher;
    $this->logger = $logger_channel_factory->get('commerce_alma');
    // If the gateway is already configured, store an API client instance.
    if (!empty($this->configuration['mode']) && !empty($this->configuration['api_key'])) {
      $this->api = new Client(
        $this->configuration['api_key'],
        ['mode' => $this->configuration['mode']]
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('event_dispatcher'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getApi(): Client {
    if ($this->api) {
      $this->api;
    }

    throw new \Exception('No Alma API client available');
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    $configuration = [
      'api_key' => '',
      'fee_plan' => '',
      'update_payments' => TRUE,
    ] + parent::defaultConfiguration();

    return $configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $form['api_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('API key'),
      '#description' => $this->t('API key for the current gateway mode.'),
      '#default_value' => $this->configuration['api_key'] ?? NULL,
    ];
    $form['fee_plan_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'fee-plan-wrapper',
      ],
    ];
    // Ajaxified fee plans selction.
    $fee_plans = [];
    if ($this->api) {
      try {
        $fee_plans = $this->api->merchants->feePlans();
      }
      catch (RequestError $e) {
      }
    }
    elseif ($values && isset($values['mode'], $values['api_key'])) {
      $api = new Client($values['api_key'], ['mode' => $values['mode']]);
      try {
        $fee_plans = $api->merchants->feePlans();
      }
      catch (RequestError $e) {
      }
    }
    $fee_plan_options = [];
    if ($fee_plans) {
      foreach ($fee_plans as $fee_plan) {
        if (!$fee_plan->allowed) {
          continue;
        }
        $fee_plan_options[$fee_plan->getPlanKey()] = $this->getFeePlanLabel($fee_plan);
      }
      $form['fee_plan_wrapper']['fee_plan'] = [
        '#type' => 'select',
        '#title' => $this->t('Fee plan'),
        '#description' => $this->t('Select an available plan.'),
        '#options' => $fee_plan_options,
        '#default_value' => $this->configuration['fee_plan'] ?? NULL,
        '#required' => TRUE,
      ];
    }
    else {
      $form['fee_plan_wrapper']['fee_plan'] = [
        '#type' => 'markup',
        '#markup' => $this->t('No available fee plan; please <strong>refresh</strong> or <strong>check your API key</strong>.'),
      ];
    }
    $form['refresh_fee_plans'] = [
      '#type' => 'submit',
      '#value' => 'Refresh fee plans',
      '#submit' => [
        [$this, 'ajaxSubmitConfigurationForm'],
      ],
      '#ajax' => [
        'callback' => [$this, 'ajaxRefreshFeePlanOptions'],
        'wrapper' => 'fee-plan-wrapper',
      ],
      '#limit_validation_errors' => [
        array_merge($form['#parents'], ['api_key']),
        array_merge($form['#parents'], ['mode']),
      ],
    ];
    $form['update_payments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update payments'),
      '#default_value' => $this->configuration['update_payments'],
    ];

    return $form;
  }

  /**
   * Ajax submit handler.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function ajaxSubmitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $form_state->setRebuild();
  }

  /**
   * Ajax callback to refresh fee plan options.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   A buildable array.
   */
  public function ajaxRefreshFeePlanOptions(array &$form, FormStateInterface $form_state): array {
    return NestedArray::getValue($form, array_merge(
      $form['#parents'],
      ['configuration', 'form', 'fee_plan_wrapper']
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      // There could be no fee plan if no refresh was triggered.
      if (!isset($values['fee_plan_wrapper']['fee_plan'])) {
        $form_state->setError($form['refresh_fee_plans'], $this->t('Please, select an available fee plan.'));
      }
      // Validate the API key.
      try {
        new Client($values['api_key'], ['mode' => $values['mode']]);
      }
      catch (ParamsError $e) {
        $form_state->setError($form['api_key'], $this->t('Invalid secret key.'));
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_key'] = trim($values['api_key']);
      $this->configuration['fee_plan'] = $values['fee_plan_wrapper']['fee_plan'];
      $this->configuration['update_payments'] = $values['update_payments'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request): void {
    // Alma add a "pid" query parameter.
    if (!$remote_payment_id = $request->query->get('pid')) {
      throw new PaymentGatewayException();
    }

    $remote_payment = $this->api->payments->fetch($remote_payment_id);
    $this->validatePayment($remote_payment);
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request): JsonResponse {
    // Alma add a "pid" query parameter.
    if (!$remote_payment_id = $request->query->get('pid')) {
      throw new PaymentGatewayException();
    }

    $remote_payment = $this->api->payments->fetch($remote_payment_id);
    $payment = $this->validatePayment($remote_payment);

    // Update order payment.
    $payment
      ->setRemoteState($remote_payment->state)
      ->setAmount((new Price((string) $remote_payment->purchase_amount, 'EUR'))->divide('100'));
    // Update the payment state according to the remote state.
    // State update is also performed through a provided queue worker.
    /** @see \Drupal\commerce_alma\Plugin\QueueWorker\PaymentUpdater */
    if ($remote_payment->state === RemotePayment::STATE_PAID) {
      $transition_id = 'authorize_capture';
    }
    else {
      $transition_id = 'authorize';
    }
    $payment->getState()->applyTransitionById($transition_id);
    $payment->save();

    return new JsonResponse();
  }

  /**
   * Validates a remote payment.
   *
   * @param Alma\API\Entities\Payment $remote_payment
   *   The remote payment.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The related payment entity.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *
   * @see https://docs.almapay.com/docs/custom-integration-technical-guide#validation-du-paiement
   */
  protected function validatePayment(RemotePayment $remote_payment): PaymentInterface {
    // The payment should already exist in a "new" state.
    /** @see \Drupal\commerce_alma\Plugin\Commerce\PaymentGateway\OffsitePaymentGateway::createPayment(). */
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->loadByRemoteId($remote_payment->id);
    if (!$payment) {
      throw new PaymentGatewayException(
        'No new order payment found for Alma payment #' . $remote_payment->id
      );
    }
    // Pay only non canceled orders, and only once.
    $order = $payment->getOrder();
    if ($order->isPaid() || $order->getState()->getId() === 'canceled') {
      return $payment;
    }
    // Check remote payment state.
    $valid_states = [
      RemotePayment::STATE_IN_PROGRESS,
      RemotePayment::STATE_PAID,
    ];
    if (!in_array($remote_payment->state, $valid_states)) {
      try {
        $this->api->payments->flagAsPotentialFraud(
          $remote_payment->id,
          RemotePayment::FRAUD_STATE_ERROR
        );
      } catch (RequestError $e) {
        $this->logger->warning($e->getErrorMessage());
      }

      throw new PaymentGatewayException(
        'Alma payment (#' . $remote_payment->id . ') state "'
        . $remote_payment->state . '" not authorized.'
      );
    }
    // Compare remote payment amount with the order payment one.
    $remote_amount = (new Price((string) $remote_payment->purchase_amount, 'EUR'))->divide('100');
    if ($remote_amount->compareTo($payment->getAmount()) !== 0) {
      try {
        $this->api->payments->flagAsPotentialFraud(
          $remote_payment->id,
          RemotePayment::FRAUD_AMOUNT_MISMATCH
        );
      } catch (RequestError $e) {
        $this->logger->warning($e->getErrorMessage());
      }

      throw new PaymentGatewayException(
        'Alma payment (#' . $remote_payment->id . ') amount '
        . 'does not match the order payment (# ' . $payment->id() . ') amount. '
        . $remote_amount->getNumber() . ' vs ' . $payment->getAmount()->getNumber() . '.'
      );
    }
    // The payment amount should not exceed the order balance.
    if ($remote_amount->compareTo($order->getBalance()) === 1) {
      throw new PaymentGatewayException(
        'Alma payment (#' . $remote_payment->id . ') exceeds the balance '
        . 'of the associated order (#' . $order->id() . ').'
      );
    }

    return $payment;
  }

  /**
   * Checks order eligibility to the configured plan.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   Eligible plans.
   */
  public function checkOrderEligibility(OrderInterface $order): array {
    $params = $eligibilities = [];
    $params['purchase_amount'] = $order->getTotalPrice()->multiply('100')->getNumber();
    $profiles = $order->collectProfiles();
    // Add optional billing address if provided.
    if (isset($profiles['billing'])) {
      $billing_address = $profiles['billing']->get('address')->first()->getValue();
      $params['billing_address'] = [
        'country' => $billing_address['country_code'],
      ];
    }
    // Add optional shipping address if provided.
    if (isset($profiles['shipping'])) {
      $shipping_address = $profiles['shipping']->get('address')->first()->getValue();
      $params['shipping_address'] = [
        'country' => $shipping_address['country_code'],
      ];
    }
    // Add queries according to the selected plan.
    // @see \Alma\API\Entities\PaymentPlanTrait::getPlanKey().
    [
      $kind,
      $installments_count,
      $deferred_days,
      $deferred_months,
    ] = explode('_', $this->configuration['fee_plan']);
    $params['queries'][] = [
      'installments_count' => (int) $installments_count,
      'deferred_days' => (int) $deferred_days,
      'deferred_months' => (int) $deferred_months,
    ];
    $eligibilities = $this->api->payments->eligibility($params, TRUE);

    return $eligibilities;
  }

  /**
   * Creates the remote payment and populate the order payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param string $return_url
   *   The retrun URL.
   * @param string $cancel_url
   *   The cancel URL.
   *
   * @return \Alma\API\Entities\Payment
   *   The Alma payment
   */
  public function createPayment(PaymentInterface $payment, string $return_url, string $cancel_url): RemotePayment {
    $order = $payment->getOrder();
    $params = [
      'origin' => 'online',
      'order' => [
        'merchant_url' => Url::fromRoute('entity.commerce_order.canonical', [
          'commerce_order' => $order->id(),
        ], ['absolute' => TRUE])->toString(),
      ],
      'payment' => [
        'return_url' => $return_url,
        'customer_cancel_url' => $cancel_url,
        'ipn_callback_url' => $this->getNotifyUrl()->toString(),
        'purchase_amount' => (int) $payment->getAmount()->multiply('100')->getNumber(),
        'locale' => $order->language()->getId(),
      ],
    ];
    // Add the customer order URL if the user is logged in.
    if ($customer_id = $order->getCustomerId()) {
      $params['order']['customer_url'] = Url::fromRoute('entity.commerce_order.user_view', [
        'user' => $customer_id,
        'commerce_order' => $order->id(),
      ], ['absolute' => TRUE])->toString();
    }

    $profiles = $order->collectProfiles();
    // Add optional billing address if provided.
    // Also, add optional customer info. from the billing profile.
    if (isset($profiles['billing'])) {
      $billing_address = $profiles['billing']->get('address')->first()->getValue();
      $company = $billing_address['organization'];
      $params['payment']['billing_address'] = [
        'company' => $company,
        'first_name' => $billing_address['given_name'],
        'last_name' => $billing_address['family_name'],
        'line1' => $billing_address['address_line1'],
        'line2' => $billing_address['address_line2'],
        'postal_code' => $billing_address['postal_code'],
        'city' => $billing_address['locality'],
        'country' => $billing_address['country_code'],
      ];
      $params['customer'] = [
        'first_name' => $billing_address['given_name'],
        'last_name' => $billing_address['family_name'],
        'email' => $order->getEmail(),
        'addresses' => [
          [
            'company' => $company,
            'first_name' => $billing_address['given_name'],
            'last_name' => $billing_address['family_name'],
            'line1' => $billing_address['address_line1'],
            'line2' => $billing_address['address_line2'],
            'postal_code' => $billing_address['postal_code'],
            'city' => $billing_address['locality'],
            'country' => $billing_address['country_code'],
            'email' => $order->getEmail(),
          ],
        ],
      ];
      if ($company) {
        $params['customer'] = [
          'is_business' => TRUE,
          'business_name' => $company,
        ];
      }
    }
    // Add optional shipping address if provided.
    if (isset($profiles['shipping'])) {
      $shipping_address = $profiles['shipping']->get('address')->first()->getValue();
      $params['payment']['shipping_address'] = [
        'company' => $billing_address['organization'],
        'first_name' => $shipping_address['given_name'],
        'last_name' => $shipping_address['family_name'],
        'email' => $order->getEmail(),
        'line1' => $shipping_address['address_line1'],
        'line2' => $shipping_address['address_line2'],
        'postal_code' => $shipping_address['postal_code'],
        'city' => $shipping_address['locality'],
        'country' => $shipping_address['country_code'],
      ];
    }

    // Apply defined fee plans options.
    // @see \Alma\API\Entities\PaymentPlanTrait::getPlanKey().
    [
      $kind,
      $installments_count,
      $deferred_days,
      $deferred_months,
    ] = explode('_', $this->configuration['fee_plan']);
    $params['payment']['installments_count'] = (int) $installments_count;
    if ($deferred_days) {
      $params['payment']['deferred_days'] = (int) $deferred_days;
    }
    elseif ($deferred_months) {
      $params['payment']['deferred_months'] = (int) $deferred_months;
    }

    // Allow params alterations through an event subscriber.
    $event = new CreatePaymentEvent($payment, $params);
    $this->eventDispatcher->dispatch($event, Events::CREATE_PAYMENT);
    $params = $event->getParams();

    // Create the Alma payment and populate the order payment.
    try {
      $remote_payment = $this->api->payments->create($params);
      $payment
        ->setRemoteId($remote_payment->id)
        ->setRemoteState($remote_payment->state);
      $payment->save();
    }
    catch (RequestError $e) {
      throw new PaymentGatewayException($e->getErrorMessage(), $e->getCode(), $e);
    }

    return $remote_payment;
  }

  /**
   * {@inheritDoc}
   */
  public function canRefundPayment(PaymentInterface $payment, Price $amount = NULL) {
    try {
      $this->api->payments->fetch($payment->getRemoteId());
      $can_refund = TRUE;
    }
    catch (RequestError $e) {
      $can_refund = FALSE;
    }

    return $can_refund;
  }

  /**
   * {@inheritDoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, static::REFUNDABLE_PAYMENT_STATES);
    $amount = $amount ?? $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);
    // Prepare patial or full refund.
    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $partial = TRUE;
    }

    try {
      // Refund remotely.
      if ($partial) {
        $this->api->payments->partialRefund(
          $payment->getRemoteId(),
          (float) $amount->multiply('100')->getNumber()
        );
      }
      else {
        $this->api->payments->fullRefund($payment->getRemoteId());
      }
      // Update order payment.
      $payment
        ->setState($partial ? 'partially_refunded' : 'refunded')
        ->setRefundedAmount($new_refunded_amount);
      $payment->save();
    }
    catch (RequestError $e) {
      throw new PaymentGatewayException($e->getErrorMessage(), $e->getCode(), $e);
    }
  }

  /**
   * Gets a fee plan label.
   *
   * @param \Alma\API\Entities\PaymentPlanInterface $fee_plan
   *   The fee plan.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The plan label.
   */
  protected function getFeePlanLabel(PaymentPlanInterface $fee_plan): MarkupInterface {
    $label = $this->t('Payment in @installments_count times.', [
      '@installments_count' => $fee_plan->getInstallmentsCount(),
    ]);
    $fee_plan_delay_d = $fee_plan->getDeferredDays();
    $fee_plan_delay_m = $fee_plan->getDeferredMonths();
    if ($fee_plan_delay_d || $fee_plan_delay_m) {
      $label .= $this->t('Deferred of @fee_plan_delay_m month(s) and @fee_plan_delay_d day(s).', [
        '@fee_plan_delay_m' => $fee_plan_delay_m,
        '@fee_plan_delay_d' => $fee_plan_delay_d,
      ]);
    }

    return $label;
  }

}
