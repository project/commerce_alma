<?php

declare(strict_types = 1);

namespace Drupal\commerce_alma\Plugin\Commerce\PaymentGateway;

use Alma\API\Client;
use Drupal\Core\Form\FormStateInterface;
use Alma\API\Entities\Payment as RemotePayment;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;

/**
 * Provides the Alma offsite Checkout payment gateway interface.
 */
interface OffsitePaymentGatewayInterface extends OffsitePaymentGatewayInterface, OffsitePaymentGatewayBase, SupportsRefundsInterface {

  const REFUNDABLE_PAYMENT_STATES = ['completed', 'partially_refunded'];

  /**
   * {@inheritDoc}
   */
  public function getApi(): ?Client;

  /**
   * Ajax submit handler.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function ajaxSubmitConfigurationForm(array &$form, FormStateInterface $form_state): void;

  /**
   * Ajax callback to refresh fee plan options.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   A buildable array.
   */
  public function ajaxRefreshFeePlanOptions(array &$form, FormStateInterface $form_state): array;

  /**
   * Checks order eligibility to the configured plan.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   Eligible plans.
   */
  public function checkOrderEligibility(OrderInterface $order): array;

  /**
   * Creates the remote payment and populate the order payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param string $return_url
   *   The retrun URL.
   *
   * @return \Alma\API\Entities\Payment
   *   The Alma payment
   */
  public function createPayment(PaymentInterface $payment, string $return_url): RemotePayment;

}
