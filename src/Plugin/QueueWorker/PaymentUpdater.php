<?php

declare(strict_types = 1);

namespace Drupal\commerce_alma\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Alma\API\Entities\Payment as RemotePayment;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Updates Alma payments.
 *
 * @QueueWorker(
 *  id = "commerce_alma_payment_updater",
 *  title = @Translation("Update Alma payments."),
 *  cron = {"time" = 60}
 * )
 */
class PaymentUpdater extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The module logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger_factory->get('commerce_alma');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
    );
  }

  /**
   * Fetches the remote payment and update order payment accordingly.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   */
  public function processItem($payment): void {
    assert($payment instanceof PaymentInterface);
    if (!$payment_gateway = $payment->getPaymentGateway()) {
      return;
    }

    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway_plugin->getConfiguration()['update_payments']) {
      return;
    }

    try {
      $remote_payment = $payment_gateway_plugin->getApi()->payments->fetch($payment->getRemoteId());
      // Capture payment if paid in full.
      if ($remote_payment->state === RemotePayment::STATE_PAID) {
        $transition_id = 'capture';
        $payment_state = $payment->getState();
        // The payment may have been "completed" manually.
        if ($payment_state->isTransitionAllowed($transition_id)) {
          $payment->getState()->applyTransitionById('capture');
        }
      }
      // Always update the remote state.
      $payment->setRemoteState($remote_payment->state);
      $payment->save();
    }
    catch (\Exception $e) {
      $this->logger->warning('Unable to fetch Alma payment.', [
        'payment_id' => $payment->id(),
        'payment_remote_id' => $payment->getRemoteId(),
      ]);
    }
  }

}
