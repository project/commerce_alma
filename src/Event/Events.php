<?php

namespace Drupal\commerce_alma\Event;

/**
 * Defines module events.
 *
 * @see \Drupal\commerce_alma\Event\CreatePaymentEvent
 */
final class Events {

  /**
   * Name of the event fired before the remote payment creation request.
   *
   * Allow to alter request parameters.
   *
   * @Event
   *
   * @see \Drupal\commerce_alma\Event\CreatePaymentEvent
   */
  const CREATE_PAYMENT = 'commerce_alma.create_payment';

}
