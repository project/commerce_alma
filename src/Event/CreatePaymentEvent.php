<?php

namespace Drupal\commerce_alma\Event;

use Drupal\commerce\EventBase;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Defines the create_payment event.
 *
 * Allow to alter the remote payment creation parameters.
 *
 * @see \Drupal\commerce_alma\Event\Events
 */
class CreatePaymentEvent extends EventBase {

  /**
   * The payment.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * The payment request parameters.
   *
   * @var array
   */
  protected $params;

  /**
   * Constructs a new PaymentEvent.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param array $params
   *   The payment request parameters.
   */
  public function __construct(PaymentInterface $payment, array $params) {
    $this->payment = $payment;
    $this->params = $params;
  }

  /**
   * Gets the payment.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The payment.
   */
  public function getPayment() {
    return $this->payment;
  }

  /**
   * Gets payment request parameters.
   *
   * @return array
   *   The payment request parameters..
   */
  public function getParams() {
    return $this->params;
  }

  /**
   * Sets payment request parameters.
   *
   * @return self
   *   The current event instance.
   */
  public function setParams(array $params): self {
    $this->params = $params;

    return $this;
  }

}
